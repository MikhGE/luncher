
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	База = Параметры.База;
	
	Отправить = 1;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	Отрисовать();
КонецПроцедуры


&НаКлиенте
Процедура Отрисовать()
	Если Отправить = 1 Тогда
		Элементы.Получатели.Видимость = Ложь;
		Элементы.Получатель.Видимость = Истина;
	Иначе
		Элементы.Получатели.Видимость = Истина;
		Элементы.Получатель.Видимость = Ложь;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура Отправить(Команда)
	Если Отправить = 1 Тогда
		Если Не ЗначениеЗаполнено(Получатель) Тогда
			Сообщить("Заполните получателя");
			Возврат;
		КонецЕсли;
	Иначе
		Если Получатели.Количество() = 0 Тогда
			Сообщить("Заполните получателей");
			Возврат;
		КонецЕсли;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(База) Тогда
		Сообщить("Выбирите базу, или группу для отправки");
		Возврат;
	КонецЕсли;
	
	ОтправитьДанныеНаСервере();
	
	Закрыть();
КонецПроцедуры

&НаСервере
Процедура ОтправитьДанныеНаСервере()
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	ИнформационныеБазы.Ссылка КАК Ссылка,
	               |	ИнформационныеБазы.ЭтоГруппа
	               |ИЗ
	               |	Справочник.ИнформационныеБазы КАК ИнформационныеБазы
	               |ГДЕ
	               |	ИнформационныеБазы.Ссылка В ИЕРАРХИИ(&Ссылка)
	               |АВТОУПОРЯДОЧИВАНИЕ";
	Запрос.УстановитьПараметр("Ссылка", База);
	
	ДеревоЗначений = Запрос.Выполнить().Выгрузить(ОбходРезультатаЗапроса.ПоГруппировкамСИерархией);
	ДеревоЗначений.Колонки.Добавить("Код");
	ДеревоЗначений.Колонки.Добавить("Данные");
	
	РекурсивноОбойтиДерево(ДеревоЗначений);
	
	ДеревоЗначений.Колонки.Удалить("Ссылка");
	
	Хранилище = Новый ХранилищеЗначения(ДеревоЗначений);
	
	Если Отправить = 1 Тогда
		ЗаписатьДанныеВРегистр(Хранилище, Получатель);
	Иначе
		Для Каждого Пользователь Из Получатели Цикл
			ЗаписатьДанныеВРегистр(Хранилище, Пользователь.Значение);	
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаписатьДанныеВРегистр(ДеревоЗначений, Получатель)
	Запись = РегистрыСведений.ОтправленныеБазы.СоздатьМенеджерЗаписи();
	Запись.ПользовательОтправитель = ПараметрыСеанса.ТекущийПользователь;
	Запись.ПользовательПолучатель = Получатель;
	Запись.Сообщение = Сообщение;
	Запись.ДеревоЗначений = ДеревоЗначений;
	Запись.НомерОтправления = РегистрыСведений.ОтправленныеБазы.ПолучитьНомерОтправления(Запись.ПользовательОтправитель, Запись.ПользовательПолучатель); 
	
	Запись.Записать();
КонецПроцедуры

&НаСервере
Процедура РекурсивноОбойтиДерево(СтрокаДерева)
	Для Каждого СтрокаДанных Из СтрокаДерева.Строки Цикл
		РекурсивноОбойтиДерево(СтрокаДанных);
		
		СтрокаДанных.Код = Строка(СтрокаДанных.Ссылка);
		
		ОбъектДанных = СтрокаДанных.Ссылка;
		
		СтруктураДанных = Новый Структура();
		СтруктураДанных.Вставить("Код",ОбъектДанных.Код);
		
		Если Не СтрокаДанных.ЭтоГруппа Тогда
			СтруктураДанных.Вставить("СтрокаПодключения",ОбъектДанных.СтрокаПодключения);
			СтруктураДанных.Вставить("ПутьКБазе",ОбъектДанных.ПутьКБазе);
			СтруктураДанных.Вставить("ИмяСервера",ОбъектДанных.ИмяСервера);
			СтруктураДанных.Вставить("ИмяБазы",ОбъектДанных.ИмяБазы);
			СтруктураДанных.Вставить("РаположенаНаСервере",ОбъектДанных.РаположенаНаСервере);
		КонецЕсли;
		
		СтрокаДанных.Данные = СтруктураДанных;
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура ОдиночноеОтправлениеПриИзменении(Элемент)
	Отрисовать();
КонецПроцедуры



