
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ВерсияЗапуска = Параметры.ВерсияЗапуска;
	РежимЗапуска = Параметры.РежимЗапуска;
	ВариантАутентификации = Параметры.ВариантАутентификации;
	СтрокаПараметров = Параметры.СтрокаПараметров;
	ИмяНастройки = Параметры.ИмяНастройки;  
	ИмяНастройкиСтарое = ИмяНастройки;
	База = Параметры.База;
	
	Изменение = Параметры.Свойство("Изменение");
	
	Если ИмяНастройки = "Стандартная" Тогда
		ИмяНастройки = "";		
	КонецЕсли;
	
	Если Параметры.ЗапускатьПредприятие Тогда
		Элементы.Предприятие.КнопкаПоУмолчанию = Истина;
		Элементы.Конфигуратор.КнопкаПоУмолчанию = Ложь;
		
		ЗапускатьПредприятие = Истина;	
	Иначе
		Элементы.Предприятие.КнопкаПоУмолчанию = Ложь;
		Элементы.Конфигуратор.КнопкаПоУмолчанию = Истина;
		
		ЗапускатьПредприятие = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Предприятие(Команда)
	Элементы.Предприятие.КнопкаПоУмолчанию = Истина;
	Элементы.Конфигуратор.КнопкаПоУмолчанию = Ложь;
	
	ЗапускатьПредприятие = Истина;
КонецПроцедуры

&НаКлиенте
Процедура Конфигуратор(Команда)
	Элементы.Предприятие.КнопкаПоУмолчанию = Ложь;
	Элементы.Конфигуратор.КнопкаПоУмолчанию = Истина;
	
	ЗапускатьПредприятие = Ложь;
КонецПроцедуры


&НаКлиенте
Процедура Сохранить(Команда)
	Если ПроверитьЗаполнение() Тогда
		СохранитьНаСервере();
		Закрыть(Новый Структура("ИмяНастройкиСтарое, ИмяНастройки",ИмяНастройкиСтарое, ИмяНастройки));
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура СохранитьНаСервере()

	НоваяЗапись = РегистрыСведений.НастройкиЗапускаБаз.СоздатьМенеджерЗаписи();
	НоваяЗапись.База = База;
	
	Если Изменение Тогда
		НоваяЗапись.Наименование = ИмяНастройкиСтарое;	
		НоваяЗапись.Прочитать();
		
		Если НоваяЗапись.Выбран() Тогда
			НоваяЗапись.Удалить();
		КонецЕсли;
		
		НоваяЗапись = РегистрыСведений.НастройкиЗапускаБаз.СоздатьМенеджерЗаписи();
		НоваяЗапись.База = База;

	КонецЕсли;
	
	НоваяЗапись.Наименование = ИмяНастройки;
		
	СтруктураПраметров = Новый Структура;
	СтруктураПраметров.Вставить("ВерсияЗапуска", ВерсияЗапуска);
	СтруктураПраметров.Вставить("РежимЗапуска", РежимЗапуска);
	СтруктураПраметров.Вставить("ВариантАутентификации", ВариантАутентификации);
	СтруктураПраметров.Вставить("СтрокаПараметров", СтрокаПараметров);
	СтруктураПраметров.Вставить("ЗапускатьПредприятие", Элементы.Предприятие.КнопкаПоУмолчанию);
	СтруктураПраметров.Вставить("ЗапускатьСразу", ЗапускатьПриОткрытии);
	
	НоваяЗапись.ЗначениеПараметров = Новый ХранилищеЗначения(СтруктураПраметров);
	
	НоваяЗапись.Записать();
КонецПроцедуры

